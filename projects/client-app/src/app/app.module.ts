import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AdsCommToolbarModule } from '@adsystem/common';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AdsCommToolbarModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
