import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdsCommToolbarComponent } from './ads-comm-toolbar.component';

@NgModule({
  declarations: [AdsCommToolbarComponent],
  imports: [CommonModule],
  exports: [AdsCommToolbarComponent],
})
export class AdsCommToolbarModule {}
