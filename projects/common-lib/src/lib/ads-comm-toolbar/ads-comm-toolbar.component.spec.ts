import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdsCommToolbarComponent } from './ads-comm-toolbar.component';

describe('AdsCommToolbarComponent', () => {
  let component: AdsCommToolbarComponent;
  let fixture: ComponentFixture<AdsCommToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdsCommToolbarComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdsCommToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
