# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.14](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.13...v0.0.14) (2020-04-17)

**Note:** Version bump only for package angular-design-system

## [0.0.13](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.12...v0.0.13) (2020-04-16)

**Note:** Version bump only for package angular-design-system

## [0.0.12](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.11...v0.0.12) (2020-04-16)

**Note:** Version bump only for package angular-design-system

## [0.0.11](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.10...v0.0.11) (2020-04-16)

**Note:** Version bump only for package angular-design-system

## [0.0.10](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.9...v0.0.10) (2020-04-16)

**Note:** Version bump only for package angular-design-system

## [0.0.9](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.8...v0.0.9) (2020-04-16)

**Note:** Version bump only for package angular-design-system

## [0.0.8](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.7...v0.0.8) (2020-04-16)

**Note:** Version bump only for package angular-design-system

## [0.0.7](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.6...v0.0.7) (2020-04-16)

**Note:** Version bump only for package angular-design-system

## [0.0.6](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.5...v0.0.6) (2020-04-16)

**Note:** Version bump only for package angular-design-system

## [0.0.5](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.4...v0.0.5) (2020-04-16)

**Note:** Version bump only for package angular-design-system

## [0.0.4](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.3...v0.0.4) (2020-04-16)

**Note:** Version bump only for package angular-design-system

## [0.0.3](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.2...v0.0.3) (2020-04-12)

### Bug Fixes

- adjustments ([56bc28f](https://gitlab.com/diegoavieira/angular-design-system/commit/56bc28f61b31189808b570f796a48b6d750ceef4))

## [0.0.2](https://gitlab.com/diegoavieira/angular-design-system/compare/v0.0.1...v0.0.2) (2020-04-12)

**Note:** Version bump only for package angular-design-system

## 0.0.1 (2020-04-12)

### Features

- created app and library ([2384b6c](https://gitlab.com/diegoavieira/angular-design-system/commit/2384b6c36b459c151da066ecd8508e4db9360412))
